# README
This is a Ruby application with a REST API that will calculate realtime statistics from the last 60 seconds. The REST API will have two endpoints: one to receive new transaction data and one to return the statistics based on the transactions made in the last 60 seconds.

I aimed to implement a web application in Ruby that can:
* receive webhook notifications containing transaction data
* process the notification data and store it in a suitable database
* display the following statistics for the last 60 seconds:
    - sum - the total sum of transaction value
    - avg - the average amount of transaction value
    - max - the single highest transaction value
    - min - the single lowest transaction value
    - count - an integer specifying total number of transactions

Lightweight: I attempted to make this application as lightweight as possible. Steering clear of Rails I decided to use Ruby with Sinatra and a NoSQL database.

Performance: I chose a Redis database to optimise performance. However Redis has its limitations and given more time I'd use a threadsafe database, like MongoDB. If I were to build out the application, I'd look to implement background workers that could process the transactions in the background.

To extend this, I would like to add security checks to ensure incoming notifications are from an authorised source before processing them.

As it's concievable that we receive multiple notifications for the same transaction, it would be a good idea to use notification_ids. We could store these in a Redis Set (only permits unique values) and check if the notification_id already exists (SISMEMBER is O(1)) before storing the transaction.

Precision: as we are dealing with money values, I used BigDecimal to ensure precision.

Testing Framework: MiniTest

#### N.B. The API does not accept notifications with timestamps older than 60 seconds or zero-value transactions.

### How To Set Up Locally - running on Ruby 2.4.1
In your terminal run:
```sh
    $ git clone https://gitlab.com/mikemiller/transactions_app.git
    $ cd transactions_app
```
run the following command to install all required gems:
```sh
    $ bundle install
```
then:
```sh
    $ redis-server
```
then:
```sh
    $ shotgun app/api/v1.rb
```
to run the tests...in a new terminal window:
```sh
    $ guard
```
Guard will run the corresponding tests when you save a file.
### To use the automated transaction notifications app for testing - download and setup [Go](https://golang.org/doc/install).
Then run:
```sh
    $ go run transaction_notifications.go http://localhost:9393/transactions
```
