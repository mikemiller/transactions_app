require_relative '../../test_helper.rb'
require 'JSON'

describe 'Api::v1' do
    before do
        $redis.DEL('transactions')
        @valid_timestamp = (Time.now.to_i - 30)
        $redis.HMSET("1:transactions", 'amount', 9.67, 'timestamp', @valid_timestamp)
        $redis.SADD('transactions', 1)
    end

    it 'should return json on get transactions' do
        get '/transactions'
        assert_equal 'application/json', last_response.headers['Content-Type']
        transaction_hash = { sum: 9.67, avg: 9.67, max: 9.67, min: 9.67, count: 1 }
        transaction_hash.each do |key, value|
            response = JSON.parse(last_response.body)
            assert_equal value, response[key.to_s]
        end
    end

    it 'should return 200 on post valid transaction ' do
        post '/transactions', { :Amount =>  9.67, :Timestamp => @valid_timestamp }.to_json
        assert_equal 200, last_response.status
        id = $redis.SMEMBERS('transactions').last
        assert_equal $redis.HGETALL("#{id}:transactions"), ['amount', '9.67', 'timestamp', @valid_timestamp.to_s]
    end

    it 'should return 204 on post transaction older than 60 seconds' do
        invalid_timestamp = @valid_timestamp - 35
        post '/transactions', { :Amount =>  9.67, :Timestamp => invalid_timestamp }.to_json
        assert_equal 204, last_response.status
    end

    it 'should ignore zero value transactions' do
        post '/transactions', { :Amount =>  0.00, :Timestamp => @valid_timestamp }.to_json
        assert_equal 400, last_response.status
        get '/transactions'
        response = JSON.parse(last_response.body)
        refute_equal 0.0, response['min']
    end

    it 'should return 400 on invalid json' do
        post '/transactions', { :Amount =>  4.55, :Timestamp => @valid_timestamp }
        assert_equal 400, last_response.status
        response = JSON.parse(last_response.body)
        assert_equal 'Invalid JSON', response['message']
    end
end
