require_relative '../test_helper.rb'

describe 'Api::helper' do
    it 'sum_amount should return an accurate float' do
        array = [0.50, 1.00, 3.00]
        result = sum_amount(array)
        assert result.is_a? Float
    end

    it 'mean_average should return an accurate float' do
        sum = 3.12
        count = 3
        result = mean_average(sum, count)
        assert result.is_a? Float
    end
end
