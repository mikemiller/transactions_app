ENV['RACK_ENV'] = 'test'
require 'minitest/autorun'
require 'rack/test'

require_relative '../app/api/v1.rb'

include Rack::Test::Methods

def app
    Sinatra::Application
end
