require 'sinatra'
require 'redis'
require 'JSON'
require_relative 'helper'

$redis = Redis.new

before do
    content_type 'application/json'
end

get '/transactions' do
    @transactions = []
    transaction_ids = $redis.SMEMBERS('transactions')
    return { message: 'no transactions yet' } if transaction_ids.empty?
    transaction_ids.each do |id|
        if $redis.hgetall("#{id}:transactions").empty?
            $redis.SREM('transactions', id)
            next
        end
        @transactions << $redis.hgetall("#{id}:transactions").to_json
    end
    count = @transactions.count
    amounts = @transactions.map { |transaction| JSON.parse(transaction)['amount'].to_f }
    sum = sum_amount(amounts)
    avg = mean_average(sum, count)
    max = amounts.max
    min = amounts.min
    { sum: sum,
      avg: avg,
      max: max,
      min: min,
      count: count }.to_json
end

post '/transactions' do
    begin
        payload = JSON.parse(request.body.read)
        return 400 unless payload['Amount'].positive?
        return 204 unless payload['Timestamp'] > (Time.now.to_i - 60)
        id = $redis.incr('id:transactions')
        $redis.HMSET("#{id}:transactions", 'amount', payload['Amount'], 'timestamp', payload['Timestamp'])
        $redis.EXPIRE("#{id}:transactions", 60)
        $redis.SADD('transactions', id)
        return 200
    rescue JSON::ParserError
        return 400, { message: 'Invalid JSON' }.to_json
    end
end
