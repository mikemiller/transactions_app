require 'bigdecimal'
require 'bigdecimal/util'

def sum_amount(amounts)
    amounts.sum.to_d.round(2).to_f
end

def mean_average(sum, count)
    (sum / count).to_d.round(2).to_f
end
