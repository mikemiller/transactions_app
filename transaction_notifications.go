package main

import (
    "bytes"
    "encoding/json"
    "log"
    "math/rand"
    "net/http"
    "os"
    "time"
)

type Event struct {
    Amount   float64
    Timestamp int64
}

func randomAmount() float64 {
    amounts := []float64{
        float64(3.95),
        float64(9.67),
        float64(43.25),
        float64(100.05),
        float64(99.98),
    }

    return amounts[rand.Intn(len(amounts))]
}

func sendRequests(target string) {
    for {
        event := Event{
            randomAmount(),
            time.Now().Unix(),
        }
        eventJson, _ := json.Marshal(event)
        resp, err := http.Post(
            target,
            "text/json",
            bytes.NewReader(eventJson),
        )

        if err != nil {
            log.Fatal(err)
        }

        if resp.StatusCode != 200 {
            log.Fatalf("Expecting 200 returned, got %f", resp.StatusCode)
        }

        time.Sleep(1 * time.Second)
    }
}

func main() {
    if len(os.Args) != 2 {
        log.Fatal("Invalid number of arguments. Expecting 1 argument, the target for notifications.")
    } else {
        target := os.Args[1]
        log.Printf("Sending events to: %s", target)
        sendRequests(target)
    }
}
